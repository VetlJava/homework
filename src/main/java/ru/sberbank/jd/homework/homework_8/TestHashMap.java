package ru.sberbank.jd.homework.homework_8;

import java.util.HashMap;

public class TestHashMap {

    public static void main(String[] args) {

        HashMap map = new HashMap();
        map.put("1", "Запись_1");
        map.put("2", "Запись_2");

        //извлекаем данные
        System.out.println(map.get("1"));
        //попробуем найти то, чего нет
        System.out.println(map.getOrDefault("4", "Значение не найдено"));
        //попробуем найти через get()....получим null
        System.out.println(map.get("4"));

        map.put(null, "Запись_null");
        System.out.println(map.get(null));
    }
}
