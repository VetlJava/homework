package ru.sberbank.jd.homework.homework_8;

import java.util.ArrayList;

public class TestArrayList {

    public static void main(String[] args) {
        ArrayList list = new ArrayList();

        list.add("Первый_1");
        System.out.println("Добавлена записть: " + list.get(0));
        System.out.println("Размер массива: " + list.size());

        list.add("Первый_2");
        System.out.println("Добавлена записть: " + list.get(1));
        System.out.println("Размер массива: " + list.size());

        list.add(2,"Первый_3");
        System.out.println("Добавлена записть: " + list.get(2));
        System.out.println("Размер массива: " + list.size());

        System.out.println("Элементы массива 1: " + list);

        //добавим элент в ячейку которая уже есть запись
        //данные не перезапишутся, а сдвинуться
        list.add(2,"Первый_X");
        System.out.println("Элементы массива 1: " + list);

        //метод addAll()
        ArrayList list2 = new ArrayList();
        list2.add("Второй_1");
        list2.add("Второй_2");
        //добавим значения из list2 в list
        list.addAll(list2);
        System.out.println("Элементы массива 1: " + list);

        //удалим элемент 2 по индексу
        list.remove(2);
        System.out.println("Элементы массива 1: " + list);
        //проверим наличие удаленного элемента
        if (list.contains("Первый_X"))
        System.out.println("Элемент не удален");
        else {
            System.out.println("Элемента нет");
        }
        //удалим конкретный элемент - "Первый_2"
        list.remove("Первый_2");
        //удалиться только первое похожее значение
        System.out.println("Элементы массива 1: " + list);
        //удалим элементы вторго массива из первого
        list.removeAll(list2);
        System.out.println("Элементы массива 1: " + list);

        //очистим список
        list.clear();
        System.out.println("Элементы массива 1: " + list);

    }
}
