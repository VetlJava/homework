package ru.sberbank.jd.homework.homework_5.factory_method;

public enum EnumCar {
    VW_POLO,
    VW_TIGUAN,
    NEW_VW_TIGUAN;
}
