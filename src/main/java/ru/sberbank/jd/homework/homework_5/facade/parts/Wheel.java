package ru.sberbank.jd.homework.homework_5.facade.parts;

public class Wheel {

    public void turn() {
        System.out.println("Колесо крутиться");
    }
}
