package ru.sberbank.jd.homework.homework_5.adapter;

public class Application {

    public static void main(String[] args) {
        AmericanSocket socket = new SimpleAmericanSocket();
        Phone phone = new Phone();
        EuroSocket euroSocket = new SocketAdapter(socket);
        phone.listMusic(euroSocket);

    }
}

