package ru.sberbank.jd.homework.homework_5.facade.objects;

import ru.sberbank.jd.homework.homework_5.facade.facade.CarFacade;
import ru.sberbank.jd.homework.homework_5.facade.parts.Door;
import ru.sberbank.jd.homework.homework_5.facade.parts.Ignition;
import ru.sberbank.jd.homework.homework_5.facade.parts.Wheel;

public class Client {

    public static void main(String[] args) {
        Door door = new Door();
        door.open();    //открыть дверь

        Ignition ignition = new Ignition();
        ignition.on();  //включение зажигания

        Wheel wheel = new Wheel();
        wheel.turn();   //колесо крутиться

        //вызов фасада
        CarFacade carFacade = new CarFacade();
        carFacade.go();
    }
}
