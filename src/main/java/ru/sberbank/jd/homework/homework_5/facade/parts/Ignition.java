package ru.sberbank.jd.homework.homework_5.facade.parts;

public class Ignition {

    public void on() {
        System.out.println("Зажигание ключено");
    }
}
