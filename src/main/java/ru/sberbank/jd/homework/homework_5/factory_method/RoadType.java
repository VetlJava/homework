package ru.sberbank.jd.homework.homework_5.factory_method;

public enum RoadType {
    CITY,
    OFF_ROAD,
    HIGHWAY;
}
