package ru.sberbank.jd.homework.homework_5.facade.parts;

public class Door {

    public void open() {
        System.out.println("Открыть дверь");
    }
}
