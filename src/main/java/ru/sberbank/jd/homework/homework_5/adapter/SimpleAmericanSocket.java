package ru.sberbank.jd.homework.homework_5.adapter;

public class SimpleAmericanSocket implements AmericanSocket {
    @Override
    public void getPower() {
        System.out.println("Вольтаж 110 вольт. Телефон работает, музыка играет :)");
    }
}
