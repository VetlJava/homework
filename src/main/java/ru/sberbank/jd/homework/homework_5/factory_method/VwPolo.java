package ru.sberbank.jd.homework.homework_5.factory_method;

public class VwPolo implements Car {
    @Override
    public void drive() {
        System.out.println("Едет со скоростью 140 км/ч");
    }

    @Override
    public void stop() {
        System.out.println("Останавливется за 10 сек.");
    }
}
