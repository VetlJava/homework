package ru.sberbank.jd.homework.homework_5.factory_method;

public class VwTiguan implements Car{
    @Override
    public void drive() {
        System.out.println("Едет со скоростью 170 км/ч");
    }

    @Override
    public void stop() {
        System.out.println("Останавливется за 7 сек.");
    }
}
