package ru.sberbank.jd.homework.homework_5.facade.facade;

import ru.sberbank.jd.homework.homework_5.facade.parts.Door;
import ru.sberbank.jd.homework.homework_5.facade.parts.Ignition;
import ru.sberbank.jd.homework.homework_5.facade.parts.Wheel;

public class CarFacade {

    private Door door = new Door();
    private Ignition ignition = new Ignition();
    private Wheel wheel = new Wheel();


    public void go() {
        System.out.println("Фасад: ");
        door.open();
        ignition.on();
        wheel.turn();
    }
}
