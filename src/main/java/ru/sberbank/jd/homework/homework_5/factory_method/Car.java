package ru.sberbank.jd.homework.homework_5.factory_method;

public interface Car {

    void drive();
    void stop();
}
