package ru.sberbank.jd.homework.homework_5.factory_method;

//фабрика по созданию автомобилей
public class CarSelector {
    public Car getCar(RoadType roadType) {
        Car car = null;
        switch (roadType) {
            case CITY:
                car = new VwPolo();
                break;
            case OFF_ROAD:
                car = new VwTiguan();
                break;
            case HIGHWAY:
                car = new NewVwTiguan();
                break;
        }
        return car;
    }
}
