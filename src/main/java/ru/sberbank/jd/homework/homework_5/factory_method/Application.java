package ru.sberbank.jd.homework.homework_5.factory_method;

public class Application {

    public static void main(String[] args) {

        CarSelector carSelector = new CarSelector();

        Car car = carSelector.getCar(RoadType.CITY);
        System.out.println("\nДля " + RoadType.CITY + " подходит: " + EnumCar.VW_POLO);
        car.drive();
        car.stop();

        car = carSelector.getCar(RoadType.OFF_ROAD);
        System.out.println("\nДля " + RoadType.OFF_ROAD + " подходит: " + EnumCar.VW_TIGUAN);
        car.drive();
        car.stop();

        car = carSelector.getCar(RoadType.HIGHWAY);
        System.out.println("\nДля " + RoadType.HIGHWAY + " подходит: " + EnumCar.NEW_VW_TIGUAN);
        car.drive();
        car.stop();
    }
}
