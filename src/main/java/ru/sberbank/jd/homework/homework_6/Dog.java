package ru.sberbank.jd.homework.homework_6;

public class Dog {

    private  int dogsCount;

    private int paws;   //лап, это поле класса
    private int tail;   //хвост
    private String name;
    private String breet;
    private String size;    //размер собаки

    // конструктор
    public Dog() {
        dogsCount ++;
        System.out.println("Собак у нас: " + dogsCount);
    }

//    public static int getDogsCount() {
//        return dogsCount;
//    }


    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        if (size.equalsIgnoreCase("Big") ||
                size.equalsIgnoreCase("Average") ||
                size.equalsIgnoreCase("Small")) {
            this.size = size;
        } else {
            System.out.println("Size should be one of these: Big, Average or Small");
        }
    }

    public int getPaws() {
        return paws;
    }

    public void setPaws(int paws) {
        if (paws == 4) {
            this.paws = paws;
        } else {
            this.paws = 4;
            System.out.println("User tried to assign " + paws + "paws for a dog");
            System.out.println("Correct number is 4");
        }
        // return this;
    }

    public int getTail() {
        return tail;
    }

    public void setTail(int tail) {
        if (tail == 1) {
            this.tail = tail;
        } else {
            this.tail = 1;
            System.out.println("User tried to assign " + tail + "tail for a dog");
            System.out.println("Correct number is 1");
        }
    }

    public String getName() {
        return name;
    }

    public Dog setName(String name) {
        this.name = name;
        return this;
    }

    public String getBreet() {
        return breet;
    }

    public Dog setBreet(String breet) {
        this.breet = breet;
        return this;
    }

    public void bark() {
        try {
            if (size.equalsIgnoreCase("Big")) {
                System.out.println("Wof-Wof");
            } else if (size.equalsIgnoreCase("Average")) {
                System.out.println("Raf-Raf");
            } else {
                System.out.println("Tiaf-Tiaf");
            }
        }catch (NullPointerException e) {
            System.out.println("Перехвачено - NullPointerException");
        }
    }

    public void bite() {
        if (dogsCount > 2) {
            System.out.println("Вас кусают собаки");
        } else {
            bark();
        }
    }
}

