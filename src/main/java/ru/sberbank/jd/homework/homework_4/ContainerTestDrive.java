package ru.sberbank.jd.homework.homework_4;

import ru.sberbank.jd.homework.homework_4.Container;

public class ContainerTestDrive {
    public static void main(String[] args) {
        Container one = new Container();
        one.setFiles(107);

        Container two = new Container();
        two.setFiles(57);

        Container three = new Container();
        three.setFiles(7);

        System.out.println("Первый контейнер содержит: " + one.getFiles() + " файлов");
        System.out.println("Второй контейнер содержит: " + two.getFiles() + " файлов");
        System.out.println("Третий контейнер содержит: " + three.getFiles() + " файлов");
        one.content();
        two.content();
        three.content();
    }
}
