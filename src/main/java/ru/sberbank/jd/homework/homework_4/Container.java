package ru.sberbank.jd.homework.homework_4;

class Container {
    private int files;

    public int getFiles() {
        return files;
    }

    public Container setFiles(int files) {
        this.files = files;
        return this;
    }

    void content() {
        if (files > 100) {
            System.out.println("Файлов в контейнере много: " + files);
        } else if (files > 50) {
            System.out.println("Файлов в контейнере не много: " + files);
        } else {
            System.out.println("Файлов в контейнере мало: " + files);
        }
    }
}
