package ru.sberbank.jd.homework.homework_7;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        logger.debug("Начало программы");

        logger.info("Входящие параметры {} {} {}", args, "Aaa", "Bbb");
        logger.warn("Делим значение на 0");
        try {
            int notValid = 5 / 0;
        } catch (Exception ex) {
            logger.error(ex);
        }
    }
}