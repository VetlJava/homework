package ru.sberbank.jd.homework.homework_7;


import lombok.extern.log4j.Log4j2;

@Log4j2
public class Factorial {


    public static void main(String[] args) {
        log.info("Факториал 5 = {}", factorial(-5));
    }

    private static long factorial(int n) {
        if (n == 1) {
            return 1;
        }
        return n * factorial(n - 1);
    }
}
