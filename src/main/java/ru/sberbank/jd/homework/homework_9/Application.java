package ru.sberbank.jd.homework.homework_9;

import java.lang.reflect.Array;
import java.util.Collections;
import java.util.Stack;

public class Application {

    public static void main(String[] args) {

        Stack <Integer> stack = new Stack<Integer>();

        for (int i = 5; i < 15; i++ ) {
            stack.push(i);
        }

        //проверим пустой или не пустой стек
        if (!stack.empty())
            System.out.println("Стек не пустой");
        System.out.println(stack);

        //добавим новый объект в стек
        stack.push(1);
        System.out.println(stack);

        //достает элемент и удаляет его из стека pop()
        System.out.println(stack.pop());

        //добавим новый объект в стек push()
        stack.push(2);
        stack.push(77);
        stack.push(98);

        //достает элемент и НЕудаляет его из стека peek()
        System.out.println(stack.peek());

        System.out.println(stack);

        Comparable intMinStack = Collections.min(stack);
        System.out.println("Минимальное значение: " + intMinStack);

        Comparable intMaxStack = Collections.max(stack);
        System.out.println("Максимальное значение: " + intMaxStack);

    }
}
