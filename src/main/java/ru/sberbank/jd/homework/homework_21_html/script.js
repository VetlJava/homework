function handlerDisplayAboutBlock() {
    const element = document.getElementById("about");
    element.style.display = element.style.display === 'none' ? 'block':'none';
}

async function fetchData() {
    let response = await fetch('https://api.github.com/users/PaulMOSQUITO');

    if (response.ok) {
        let json = await response.json();

        let li = document.createElement('li');

        const ul = document.getElementById('tags-id');
        li.innerHTML = json.bio;
        ul.append(li);

        document.getElementById('photo').src=json.avatar_url;
        document.getElementById("name").textContent=json.login;

        
    } else {
        alert("Ошибка")
    }

}