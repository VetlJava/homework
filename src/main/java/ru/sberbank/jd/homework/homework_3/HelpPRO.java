package ru.sberbank.jd.homework.homework_3;

public class HelpPRO {
    public static void main(String[] args) throws java.io.IOException {
        char choice;

        System.out.println("Спаравка: \n 1. if\n 2. switch\n 3. for\n 4. while\n " +
                "5. Условный оператор\nВыберите: ");
        choice = (char) System.in.read();

        switch (choice) {
            case '1':
                System.out.println("Инструкция if:\n");
                System.out.println("if(условие) инструкция;");
                System.out.println("else инструкция");
                break;
            case '2':
                System.out.println("Инструкция switch:");
                System.out.println("switch(выражение) {");
                System.out.println(" case константа:");
                System.out.println(" последовательность инструкций");
                System.out.println(" break;");
                System.out.println(" // ...");
                System.out.println("}");
                break;
            case '3':
                System.out.println("Цикл for:");
                System.out.println(" for(инициализация; условие; интерация) инструкция;");
                System.out.println("\t// инициализация - значение переменной\n\t// интерация -  порядок изменения переменной" +
                        "\n\t// инструкция допускается пустая");
                break;
            case '4':
                System.out.println("Синтаксис цикла while:");
                System.out.println("while (условие) блок;");
                System.out.println("\t// условие - конкретное условие управления циклом (любое логическое выражение)" +
                        "\n\t// блок - одиночная иструкция или блок инструкций");
                break;
            case '5':
                System.out.println("Синтаксис условного оператора:");
                System.out.println("(логическое-выражение) ? если-истина-возвращаем-это : если-ложь-возвращаем-это");

                break;

            default:
                System.out.println("Запрос не найден.");
        }

    }
}