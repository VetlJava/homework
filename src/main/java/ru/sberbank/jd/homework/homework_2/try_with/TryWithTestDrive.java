package ru.sberbank.jd.homework.homework_2.try_with;

import java.io.*;

public class TryWithTestDrive {

    public static void main(String[] args)  throws IOException {
            //Поток для чтения
        try(FileInputStream fileIn = new FileInputStream("/Users/vetl.foto/" +
                "Documents/Обучение/Виртуальная школа_Сбер/REBOOT/Sberbank/Files/" +
                "Try-with-resources.txt");
            //Поток вывода
            FileOutputStream fileOut = new FileOutputStream("/Users/vetl.foto/" +
                    "Documents/Обучение/Виртуальная школа_Сбер/REBOOT/Sberbank/Files/" +
                    "New_Try-with-resources.txt");
            //Массив байтов прочтённых из файла
            ByteArrayOutputStream temp = new ByteArrayOutputStream()) {

                    int i;
                    while ((i = fileIn.read()) != -1) {   //Читаем пока не закончатся данные
                        temp.write(i);     //Запись данных в промежуточный массив
                    }
                    temp.writeTo(fileOut);  //Записываем данные в файл
        }   catch (IOException e) {
            e.printStackTrace();
        }
    }
}
