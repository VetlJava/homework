package ru.sberbank.jd.homework.homework_2.links;

public class LinkTestDrive {

    public static void main(String[] args) {
        Link A = new Link();
        A.i = 10;
        System.out.println("Переменная A = " + A.i);

        Link B = A;
        System.out.println("Переменная B = " + B.i);

        B.i = 50;
        System.out.println("После присвоения нового значение B, A теперь содержит = " + A.i);

        rez(A);
        System.out.println("Изменённое значение A = " + A.i);
    }

    public static void rez(Link link) {        // пишем метод и присваиваем новое значение
        link.i = 100;                          // передача переменной по ссылке
    }
}
