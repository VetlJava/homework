package ru.sberbank.jd.homework.homework_12.example_2.service;

import ru.sberbank.jd.homework.homework_12.example_2.utils.Calculator;

public class ThreadCalculator extends Thread {

    public ThreadCalculator(String name) {
        super(name);
    }

    @Override
    public void run() {
        Calculator.sum(1000);
    }
}
