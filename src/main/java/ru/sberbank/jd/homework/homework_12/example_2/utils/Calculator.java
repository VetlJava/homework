package ru.sberbank.jd.homework.homework_12.example_2.utils;

public class Calculator {

    public static int sum(Integer timeSleep) {
        try {
            Thread.sleep(timeSleep);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int result = 0;
        for (int i = 0; i < 1000; i++) {
            result += i;
        }
        System.out.println(String.format("Имя потока = %s, сумма = %s", Thread.currentThread().getName(),result));
        return result;
    }
}
