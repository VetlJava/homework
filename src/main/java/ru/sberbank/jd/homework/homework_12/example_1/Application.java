package ru.sberbank.jd.homework.homework_12.example_1;

import ru.sberbank.jd.homework.homework_12.example_1.utils.Calculator;

//Потоки
public class Application {

    public static void main(String[] args) throws InterruptedException {
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < 10; i++) {
            Thread thread = new Thread(() -> Calculator.sum(10),
                    String.valueOf(String.format("Побочный поток %s", i)));
            thread.setDaemon(true);
            thread.setPriority(7);
            thread.start();
        }
        Calculator.sum(1);
        long endTime = System.currentTimeMillis();
        System.out.println(String.format("Программа заверщена. Время работы = %s", endTime - startTime));
    }

}
