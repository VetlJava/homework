package ru.sberbank.jd.homework.homework_12.example_2;

import ru.sberbank.jd.homework.homework_12.example_2.service.ThreadCalculator;
import ru.sberbank.jd.homework.homework_12.example_2.utils.Calculator;

import java.util.ArrayList;
import java.util.List;

//Потоки
public class Application {

    public static void main(String[] args) throws InterruptedException {
        long startTime = System.currentTimeMillis();
        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Thread thread = new ThreadCalculator(
                    String.valueOf(String.format("Побочный поток %s", i)));
            thread.setDaemon(true);
            thread.start();
            threads.add(thread);
        }

        Calculator.sum(1000); //main

        for (int i = threads.size() - 1; i >= 0; i--) {
            threads.get(i).join();
        }

        long endTime = System.currentTimeMillis();
        System.out.println(String.format("Программа заверщена. Время работы = %s", endTime - startTime));

    }
}
