package ru.sberbank.jd.homework.homework_bd.utils;

import ru.sberbank.jd.homework.homework_bd.entities.Baza;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DbUtils {

    public static Connection createConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:h2:~/test",
                "sa", "");
    }
    //метод который создает таблицу

    public static void createTable(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute("create table if not exists  baza (id text, manufacturer text, model text)");
        connection.commit();    //применение изменений

    }

    public static List<Baza> selectAll(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();

        ResultSet resultSet = statement.executeQuery("select * from baza;");

        List<Baza> bazas = new ArrayList<>();
        while (resultSet.next()) {
            String id = resultSet.getString(1);
            String manufacturer = resultSet.getString(2);
            String model = resultSet.getString(3);
            bazas.add(Baza.builder()
                    .id(id)
                    .manufacturer(manufacturer)
                    .model(model)
                    .build());
        }
        return bazas;
    }

    //добавление в таблицу
    public static void  insertDevice(Connection connection, Baza baza) throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute(String.format("insert into baza (id, manufacturer, model)" +
                            "values ('%s', '%s', '%s')", baza.getId(), baza.getManufacturer(), baza.getModel()));
        connection.commit();
    }
}
