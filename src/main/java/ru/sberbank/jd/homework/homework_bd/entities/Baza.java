package ru.sberbank.jd.homework.homework_bd.entities;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class Baza {

    private String id;
    private String manufacturer;
    private String model;
}
