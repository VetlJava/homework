package ru.sberbank.jd.homework.homework_bd;

import ru.sberbank.jd.homework.homework_bd.entities.Baza;
import ru.sberbank.jd.homework.homework_bd.utils.DbUtils;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Random;

public class Application {

    public static void main(String[] args) throws SQLException {
        Connection connection = DbUtils.createConnection();

        DbUtils.createTable(connection);

        //List<Baza> devices = DbUtils.selectAll(connection);
        System.out.println(String.format("Найдено автомобилей = %s", DbUtils.selectAll(connection)));
        DbUtils.insertDevice(connection, Baza.builder()
                .id("098")
                .model("Polo")
                .manufacturer("Vw")
                .build());
        DbUtils.insertDevice(connection, Baza.builder()
                .id("666")
                .model("TT")
                .manufacturer("Audi")
                .build());
        System.out.println(String.format("Найдено автомобилей = %s", DbUtils.selectAll(connection)));
    }
}
